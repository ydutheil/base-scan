import pyjapc
import time
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from functools import partial
from scipy import constants as ctc
from scipy import optimize
import os
import functions as fs
import itertools

setRun = False

cycleUSER = 'LNA.USER.PBPRD1'
japc = pyjapc.PyJapc(selector='', incaAcceleratorName='ELENA', noSet=(not setRun))

try:
    japc.rbacLogin()
except:
    print('**** login failed, no set possible ****')
    pass
          

plt.ion()
# window of the scan itself
fig_scan, ax_scan = plt.subplots(2, 1, figsize=(9, 5), sharex=True)
fig_scan.canvas.set_window_title('Scan')
ax_scan[0].set_ylabel('X (mm)')
ax_scan[1].set_ylabel('Y (mm)')



X_scan = np.linspace(0, 5, 10)
Y_scan = np.linspace(-20, 15, 10)
scan_iterable = list(itertools.product(X_scan, Y_scan))
set_address = ['LNE07BEAM/BASE-H-OFFSET-mm', 'LNE07BEAM/BASE-V-OFFSET-mm']

exp = fs.Experiment(
    japc, 
    scan_iterable=scan_iterable, cycleUSER=cycleUSER,
    nrep=1, set_address=set_address, 
    fig_scan=fig_scan, ax_scan=ax_scan)

japc.subscribeParam('LNE07.BSGW.0744/Acquisition#acqTime',
                    partial(fs.measurement, exp, timingSelectorOverride=cycleUSER), 
                    timingSelectorOverride=cycleUSER)
japc.startSubscriptions()
print('subscribed and running')

