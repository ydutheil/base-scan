import numpy as np
import matplotlib.pyplot as plt
import time
import random as rdm
import pandas as pd

from dateutil.parser import parse
import datetime


class Experiment:

    def __init__(self,
                 japc, 
                 scan_iterable, cycleUSER,
                 nrep, set_address,#set_address2,
                 fig_scan, ax_scan):
        self.japc = japc
        self.cycleUSER = cycleUSER
        self.set_address = set_address

        self.scan_iterable = scan_iterable
        self.nrep = nrep

        self.fig_scan = fig_scan
        self.ax_scan = ax_scan

        self.id_meas = 0
        self.id_rep = 0
        self.data = []
        self.integral_limit = 500
        self.start_time = pd.to_datetime(datetime.datetime.now())

        
        self.init_set_values = []
        for knob in self.set_address:
            self.init_set_values.append(self.japc.getParam(knob))


    def next_meas(self):
        # check that current measurement is different from previous
        try:
            if False:  # condition that the meansurement is not good
                print("current measurement has repeated, ignoring")
                self.line = []
                return
        except IndexError:
            pass

        self.time_now = pd.to_datetime(datetime.datetime.now())
        bsg = self.japc.getParam('LNE07.BSGW.0744/Acquisition#rawIntegrals', timingSelectorOverride=self.cycleUSER)
        # check that there is beam
        integral = bsg.sum()
        if integral < self.integral_limit:  # in unit of e10
            # print('******** got to small or to big sigx: {} or sigy: {}: '.format(sigmax, sigmay))
            # print('******** got to  big sigx_err: {} or sigy_err: {}: '.format(sigmax_err, sigmay_err))
            print('got too low integral', integral)
            self.line = []
            return

        self.vals = []
        for knob in self.set_address:
            self.vals.append(self.japc.getParam(knob))
            
    
        self.data.append([self.time_now, *self.vals])
        self.update_plot()
        self.line = []

        self.id_rep += 1
        if self.id_rep >= self.nrep:
            self.id_rep = 0
            self.id_meas += 1
            if self.id_meas >= len(self.scan_iterable):
                self.set_params(self.init_set_values)
                self.end_measurement()
                print('****** measurement ended')
            else:
                self.set_params(self.scan_iterable[self.id_meas])


            

    def set_params(self, values):
        for value, address in zip(values, self.set_address):
            try:
                self.japc.setParam(address, value)
            except:
                print('set failed, probably a limit reached, check output')
                pass
            
            print(address, value)

    def end_measurement(self):
        self.japc.stopSubscriptions()
        # self.set_params(self.init_set_value)  # restore starting value

        # now we set the array and do the fit
        columns = ['time', *self.set_address]
        df = pd.DataFrame(self.data, columns=columns)

        df.to_pickle('scan_' + time.strftime('%d_%m_%Y_%H_%M') + '.pckl')


    def update_plot(self):

        self.ax_scan[0].plot(self.time_now, self.vals[0], 'ro')
        self.ax_scan[1].plot(self.time_now, self.vals[1], 'ro')

        self.fig_scan.autofmt_xdate()
        self.ax_scan[1].set_xlim(self.start_time, self.time_now)
        self.fig_scan.canvas.draw()


def measurement(exp, parameter_name, new_value, **kwargs):
    print("callback", parameter_name, new_value)
    time.sleep(5)

    try:
        measurement.previous
    except:
        measurement.previous = np.NaN

    if measurement.previous == new_value:
        print('the same measurement, discard', new_value)
        return
    measurement.previous = new_value

    print('measurement {} , repetition {}'.format(exp.id_meas, exp.id_rep))
    exp.next_meas()
